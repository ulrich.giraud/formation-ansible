variable "vsphere_datacenter" {type = string}
variable "vsphere_datastore" {type = string}
variable "vsphere_cluster" {type = string}
variable "vsphere_network" {type = string}
variable "vsphere_user" {type = string}
variable "vsphere_password" {type = string}
variable "vsphere_server" {type = string}
variable "solidserver_user" {type = string}
variable "solidserver_password" {type = string}
variable "solidserver_server" {type = string}
variable "solidserver_space" {type = string}
variable "solidserver_subnet" {type = string}
variable "solidserver_pool" {type = string}

variable "machines" {
  type = list(object({
    name = string
    user_id = string
    num_cpus = optional(number, 2)
    memory = optional(number, 8192)
    template = optional(string, "template-k8s-1-24-7-alma9-x86-64")
  }))
  validation {
    condition = alltrue([
      for o in var.machines : can(regex("^[A-Z]{4}[0-9]{4}$", o.user_id))
    ])
    error_message = "All user_id fields should respect the following regex: ^[A-Z]{4}[0-9]{4}$"
  }
}