terraform {
  required_providers {
    solidserver = {
      source = "EfficientIP-Labs/solidserver"
      version = "~> 1.1.0"
    }
    macaddress = {
      source = "ivoronin/macaddress"
      version = "0.3.2"
    }
  }
}


provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
  persist_session      = true
}

provider "solidserver" {
    username  = var.solidserver_user
    password  = var.solidserver_password
    host      = var.solidserver_server
    sslverify = "false"
}