# jouer le playbook debug-varaibles.yml avec un extra var
ansible-playbook debug-variables.yml --extra-var var_test="'extra var'"
# Afficher les facts de la machine ansible-cible-1
ansible ansible-cible-1 -m setup
# Utiliser un filtre pour n’afficher que les informations sur la distribution de la machine ansible-cible-1
ansible ansible-cible-1 -m setup -a 'filter=ansible_distribution*'