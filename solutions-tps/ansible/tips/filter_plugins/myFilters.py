#!/usr/bin/python
class FilterModule(object):
    def filters(self):
        return {
            'customfilter1': self.customfilter1,
            'customfilter2': self.customfilter2
        }

    def customfilter1(self, obj):
        print(obj)
        result = []
        for r in obj:
            name = "client_" + r['name']
            result.append({ 'database': name, 'name': name, 'password': r['db_pwd'], 'roles': ['readWrite', 'dbAdmin'] })

        return result

    def customfilter2(self, obj):
        print(obj)
        result = {}

        name = "client_" + obj['name']
        result['database'] = name
        result['name'] = name
        result['password'] = obj['db_pwd']
        result['roles'] = ['readWrite', 'dbAdmin']

        return result
