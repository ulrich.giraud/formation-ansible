#executer le playbook install-curl.yml avec le user formation
ansible-playbook  install-curl.yml -e 'ansible_ssh_user=formation'

#executer le playbook install-curl.yml avec le user formation avec un prompt sudo password 
ansible-playbook  --ask-become-pass install-curl.yml -e 'ansible_ssh_user=formation' 

#créer le fichioer vault sudo.yml
ansible-vault create ./vars/sudo-pass.yml

#executer le playbook utilisateur.yml avec un prompt vault password 
ansible-playbook  --ask-vault-pass utilisateurs.yml